;;; -*- Lisp -*-

(defsystem "rpm"
  :version "0.0.2"
  :description "functions to use the RedHat Package Management system"
  :author "Francois-Rene Rideau"
  :license "MIT"
  :depends-on ("inferior-shell" "lambda-reader" "cl-ppcre" (:version "fare-utils" "1.0.0.5"))
  :components
  ((:file "pkgdcl")
   (:file "specials" :depends-on ("pkgdcl"))
   (:file "version" :depends-on ("pkgdcl"))
   (:file "upgrade" :depends-on ("pkgdcl"))))
